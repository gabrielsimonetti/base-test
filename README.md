# ReadLeads
Processa leads nos formatos CSV e JSON e retorna um JSON com as leads agrupadas por Estado e Cidade.

## Instruções

### Requerimentos

* NodeJS v8.x ou superior

### Instalação

* Fazer download do código
```
git clone git@bitbucket.org:gabrielsimonetti/base-test.git
```
* Instalar dependências
```
npm install
```
* Executar o programa

```
node readleads [URL ou caminho relativo do arquivo]
```