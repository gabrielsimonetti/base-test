### Qual a stack tecnológica que você utiliza ou gostaria de utilizar, quais linguagens e frameworks você escolheria hoje para escolher como foco?
Atualmente estou mais confortável com as seguintes tecnologias:
* Backend: PHP (Laravel, Zend) ou NodeJS
* Frontend: React, Vue e Angular 1.
* Databases: MySQL, PostgreSQL, MongoDB

Contudo, tenho interesse em aprender mais sobre Java e Kotlin (com Spring Framework) e também um pouco mais sobre linguagens funcionais  - em particular, Clojure.

### Qual foi o projeto que você realizou, profissional ou não, que te deixou mais orgulhoso?
Foi todo o projeto que desenvolvi durante o meu tempo como programador e líder técnico do site [Zelda.com.br](https://zelda.com.br). Entre 2004 e 2017 eu estive na liderança técnica do site, e ajudei a lançar vários projetos, como:
* Site em Drupal - fui o principal responsável pela migração de um site completamente estático para um dinâmico, usando o CMS Drupal;
* Zelda Translate - Uma ferramenta de tradução, na qual os membros do site podiam ajudar a traduzir os jogos da série para o Português;
* Zelda Maps - Uma ferramenta que usa a API do Google Maps para mapear os ambientes dos jogos;
* Zelda Fórum - Criei vários sistemas de gamification para os usuários do Fórum Zelda.com.br.

Essa longa bagagem me trouxe aprendizados em várias tecnologias diferentes, bem como excelentes habilidades de comunicação.

### Qual a sua experiência com a construção de APIs?
Tenho mais de quatro anos de experiência construindo várias APIs RESTful, públicas e privadas (usando tanto autenticação via sessão quanto autenticação via tokens JWT), para diversos tipos de projetos.

### Nós na BASE temos muita preocupação com a qualidade dos softwares que desenvolvemos, acreditamos que o uso de testes automatizados podem agregar muito nas nossas entregas, o que você acha disso? Qual a sua experiência com testes?
Acho uma ótima prática e que realmente agrega muito na qualidade final do projeto. Facilita a manutenção e o desenvolvimento.
 
Tenho experiência com testes unitários usando PHPUnit e MochaJS. Sempre tentei utilizar mais testes nos meus projetos, mas a maioria dos projetos em que trabalhei eram para startups que precisavam de tudo pronto "para ontem" - e por conta disso, eu não tinha muito tempo para implementar testes.

### 5. Você tem familiaridade com ferramentas de métricas como New Relic, CloudWatch e outros?
Estou ciente da existência mas nunca utilizei essas ferramentas nos meus projetos.  