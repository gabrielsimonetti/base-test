let assert = require('assert');
let LeadReader = require('../LeadReader');

describe('LeadReader', () => {
  describe('Reading file', () => {
    describe('Reading from filesystem', () => {
      it('Should fail when the file doesn\'t exist in the filesystem', done => {
        let LR = new LeadReader();
        LR.readFromFS('testFile2.csv').then(() => done(new Error())).fail( err => {
          assert.equal(err.errno, -4058, 'Correct error number');
          done();
        } );
      });
      it('Should succeed when the file exists in the filesystem', done => {
        let LR = new LeadReader();
        LR.readFromFS('testFile.csv').then(() => done()).fail(done);
      });
    });

    describe('Reading from URL', () => {
      it('Should fail when cannot connect or read the URL', done => {
        let LR = new LeadReader();
        LR.readFromUrl('https://gist.githubusercontent.com/invalidfile').then(function(a){
          console.log(a); done();
        }).fail( err => {
          assert.equal(err.errno, -404, 'Correct error number');
          done();
        } );
      });
      it('Should succeed when the connection is successful', done => {
        let LR = new LeadReader();
        LR.readFromUrl('https://gist.githubusercontent.com/adaosantos/d1a9cbb6be96f1b449bcee80f7ebf9ca/raw/5609b247bb5d225054be1c2fa653a804d5a01882/samplejson').then(() => done()).fail(done);
      });
    });
  });

  describe('Parsing file', () => {
    describe('Parsing CSV file', () => {
      it('Should fail when file is not in valid CSV format', done => {
        let LR = new LeadReader({fileFormat: 'csv'});
        LR.init('testFile.json').then(done).fail(function(err){
          assert.equal(err.errno, -324, 'Correct error number');
          done();
        })
      });
      it('Should succeed when file is in valid CSV format', done => {
        let LR = new LeadReader({fileFormat: 'csv'});
        LR.init('testFile.csv').then(() => done()).fail(done);
      });
    });
    describe('Parsing JSON file', () => {
      it('Should fail when file is not in valid JSON format', done => {
        let LR = new LeadReader({fileFormat: 'json'});
        LR.init('testFile.csv').then(done).fail(function(err){
          assert.equal(err.errno, -768, 'Correct error number');
          done();
        })
      });
      it('Should succeed when file is in valid JSON format', done => {
        let LR = new LeadReader({fileFormat: 'json'});
        LR.init('testFile.json').then(() => done()).fail(done);
      });
    });
  });

  describe('Processing and grouping leads', () => {
    describe('Check for the correct grouping of states', () => {
      it('São Paulo', done => {
        let LR = new LeadReader();
        LR.init('testFile.json').then((data) => {
          assert.ok(data['SAO PAULO'], 'Estado OK');
          assert.ok(data['SAO PAULO']['SAO PAULO'], 'Cidade OK');
          assert.equal(data['SAO PAULO']['SAO PAULO'].length, 4, 'Tamanho da Importação OK');
          done();
        }).fail(done);
      });
      it('Rio de Janeiro', done => {
        let LR = new LeadReader();
        LR.init('testFile.json').then((data) => {
          assert.ok(data['RIO DE JANEIRO'], 'Estado OK');
          assert.ok(data['RIO DE JANEIRO']['RIO DE JANEIRO'], 'Cidade OK');
          assert.equal(data['RIO DE JANEIRO']['RIO DE JANEIRO'].length, 4, 'Importação OK');
          done();
        }).fail(done);
      });
      it('Rio Grande do Sul', done => {
        let LR = new LeadReader();
        LR.init('testFile.json').then((data) => {
          assert.ok(data['RIO GRANDE DO SUL'], 'Estado OK');
          assert.ok(data['RIO GRANDE DO SUL']['PORTO ALEGRE'], 'Cidade OK');
          assert.equal(data['RIO GRANDE DO SUL']['PORTO ALEGRE'].length, 2, 'Importação OK');
          done();
        }).fail(done);
      });
    })
  });

});