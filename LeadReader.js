let fs = require("fs"),
    http = require("http"),
    https = require("https"),
    path = require("path"),
    Q = require("q"),
    csv = require("csv");

class LeadReader{
  /**
   * Instantiate the class with the options provided.
   * @param options
   */
  constructor(options){
    options = options || {};
    // The source where the file will be pulled from. Available options: [http, https, filesystem]
    this.fileSource = (options.fileSource ? options.fileSource : null);
    // The format of the file being processed. Available options: [json, csv]
    this.fileFormat = (options.fileFormat ? options.fileFormat : null);
    // The CSV separator. Available options: [";",","]
    this.csvSeparator = ';';
    this.sourceString = '';
    this.processed = {};

    return this;
  }

  /**
   * Initiates the process that reads and parses the file.
   * @param file_name
   * @returns {Promise<any | never>}
   */
  init(file_name){
    let self = this;
    return this.readSource(file_name).then(data => {
      self.sourceString = data;
      return self.parse();
    }).then(parsed => {
      parsed.forEach((a) => self.processLine(a));
      return self.processed;
    });
  }

  /**
   * Attempts to read a source
   * @param source
   * @return Promise
   */
  readSource(source){
    if(this.fileSource === 'http'){
      return this.readFromUrl(source, http);
    }
    if(this.fileSource === 'https'){
      return this.readFromUrl(source, https);
    }
    if(this.fileSource === 'filesystem'){
      return this.readFromFS(source);
    }

    if(source.toLowerCase().indexOf('https') === 0){
      return this.readFromUrl(source, https);
    }
    if(source.toLowerCase().indexOf('http') === 0){
      return this.readFromUrl(source, http);
    }
    return this.readFromFS(source);
  }

  /**
   * Parses a string containing the leads.
   * @param string
   * @return Promise
   */
  parse(){
    let string = this.sourceString;
    if(this.fileFormat === 'csv'){
      return this.parseCSV(string);
    }
    if(this.fileFormat === 'json'){
      return this.parseJSON(string);
    }

    try{
      JSON.parse(string);
      return this.parseJSON(string);
    } catch (e){
      return this.parseCSV(string);
    }
  }

  /**
   * Attempts to read a file from a source.
   * @param url
   * @returns Promise
   */
  readFromUrl(url, method){
    let deferred = Q.defer();
    let self = this;
    method = method || https;

    method.get(url, res =>{
      if(res.statusCode !== 200){
        let err = new Error();
        err.errno = -404;
        deferred.reject(err);
      }
      let data = '';
      res.on('data', chunk => {
        data += chunk;
      });
      res.on('end', () => {
        self.sourceString = data;
        deferred.resolve(data);
      });
      res.on('error', err => {
        deferred.reject(err);
      });
    });
    return deferred.promise;
  }

  /**
   * Reads a file from the filesystem, assuming the project root as main path.
   * Returns a promise that will be resolved if the file was read successfully and rejected if there was an error.
   * @param relPath
   */
  readFromFS(relPath){
    let deferred = Q.defer();
    let filePath = path.join(__dirname, relPath);
    let self = this;
    fs.readFile(filePath, {encoding: 'utf-8'}, (err, data) => {
      if(err){
        err.errno = -4058; // File does not exist on filePath
        deferred.reject(err);
      } else {
        self.sourceString = data;
        deferred.resolve(data);
      }
    });
    return deferred.promise;
  }

  /**
   * Attempts to parse a string into json. If it fails, assume it's a CSV file.
   * @returns Promise
   */
  parseJSON(str){
    let deferred = Q.defer();
    try{
      let ret = JSON.parse(str);
      deferred.resolve(ret);
    }catch(e){
      e.errno = -768;
      deferred.reject(e);
    }
    return deferred.promise;
  }

  parseCSV(str){
    let deferred = Q.defer();
    let self = this;
    csv.parse(str, {delimiter: this.csvSeparator}, (err, data) => {
      if(err){
        err.errno = -324;
        deferred.reject(err);
      } else {
        let headers = data.shift();
        let parsed = [];
        for (let index=0; index < data.length; index++){
          let newElement = {};
          for(let info = 0; info < data[index].length; info++){
            newElement[headers[info]] = data[index][info];
          }
          parsed.push(newElement);
        }
        deferred.resolve(parsed);
      }
    });
    return deferred.promise;
  }

  /**
   * Processes a single line, adding it to the array of processed elements.
   * @param sourceLine
   */
  processLine(sourceLine){
    let estado = sourceLine.estado;
    let cidade = sourceLine.cidade;
    let line = this.sanitizeLine(sourceLine);

    if(!(estado in this.processed)){
      this.processed[estado] = {};
    }

    if(!(cidade in this.processed[estado])){
      this.processed[estado][cidade] = [];
    }

    this.processed[estado][cidade].push(line);
  }

  /**
   * Sanitizes a single line and returns a clean line.
   * @param sourceLine
   */
  sanitizeLine(sourceLine){
    let line = {};

    if('nome' in sourceLine){
      line.nome = sourceLine.nome;
    }
    if('telefone' in sourceLine){
      line.telefone = sourceLine.telefone;
    }
    if('observacoes' in sourceLine){
      line.observacoes = sourceLine.observacoes;
    }

    return line;
  }
}

module.exports = LeadReader;