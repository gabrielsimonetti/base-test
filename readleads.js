let program = require("commander");
let ReadLeads = require("./LeadReader");
let fileName = '';

/**
 * Declaration and details on how to use the program
 */
program
  .usage('[options]')
  .description('Read leads from a source file and return it in json format.')
  .arguments('[fn]')
  .action(function (fn){
    fileName = fn;
  })
  .version('1.0.0')
  .option('-s, --source [source]', 'Define the source of origin. Must be one of [http,https,filesystem]')
  .option('-f, --format [format]', 'Define the format of the file being processed. Must be one of [csv,json]')
  .option('--separator [separator]', 'Define the format of the csv separator.');

program.parse(process.argv);
if (!program.args.length) program.help();

let options = {};
if(program.source){
  options.fileSource = program.source;
}

if(program.format){
  options.fileFormat = program.format;
}

if(program.separator){
  options.csvSeparator = program.separator;
}

ReadLeads = new ReadLeads(options);
ReadLeads.init(fileName).then(output => console.log(JSON.stringify(output, null, 2))).fail(console.log);